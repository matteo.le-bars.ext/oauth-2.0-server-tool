```

         ██████╗  █████╗ ██╗   ██╗████████╗██╗  ██╗    ██████╗     ██████╗             
        ██╔═══██╗██╔══██╗██║   ██║╚══██╔══╝██║  ██║    ╚════██╗   ██╔═████╗            
        ██║   ██║███████║██║   ██║   ██║   ███████║     █████╔╝   ██║██╔██║            
        ██║   ██║██╔══██║██║   ██║   ██║   ██╔══██║    ██╔═══╝    ████╔╝██║            
        ╚██████╔╝██║  ██║╚██████╔╝   ██║   ██║  ██║    ███████╗██╗╚██████╔╝            
         ╚═════╝ ╚═╝  ╚═╝ ╚═════╝    ╚═╝   ╚═╝  ╚═╝    ╚══════╝╚═╝ ╚═════╝             
                                                                                       
███████╗███████╗██████╗ ██╗   ██╗███████╗██████╗    ████████╗ ██████╗  ██████╗ ██╗     
██╔════╝██╔════╝██╔══██╗██║   ██║██╔════╝██╔══██╗   ╚══██╔══╝██╔═══██╗██╔═══██╗██║     
███████╗█████╗  ██████╔╝██║   ██║█████╗  ██████╔╝█████╗██║   ██║   ██║██║   ██║██║     
╚════██║██╔══╝  ██╔══██╗╚██╗ ██╔╝██╔══╝  ██╔══██╗╚════╝██║   ██║   ██║██║   ██║██║     
███████║███████╗██║  ██║ ╚████╔╝ ███████╗██║  ██║      ██║   ╚██████╔╝╚██████╔╝███████╗
╚══════╝╚══════╝╚═╝  ╚═╝  ╚═══╝  ╚══════╝╚═╝  ╚═╝      ╚═╝    ╚═════╝  ╚═════╝ ╚══════╝
                                                                                       
```
# Introduction

This repository allows you to build a dockerized OAuth 2.0 server tool based on [OAuth2-Server](https://oauth2-server.readthedocs.io/en/latest/index.html).

[OAuth2-server](https://npmjs.org/package/oauth2-server) is a complete, compliant and well tested module for implementing an OAuth2 server in [Node.js](https://nodejs.org/). The project is [hosted on GitHub](https://github.com/oauthjs/node-oauth2-server) and the included test suite is automatically [run on Travis CI](https://travis-ci.org/oauthjs/node-oauth2-server).

# Setup

**Prerequisites:**
- [Docker Desktop](https://www.docker.com/products/docker-desktop)

**Setup steps:**
1. Clone this repository `git clone https://gitlab.dematic.com/lebarsma/oauth-2.0-server-tool.git`
2. Open a terminal and navigate to the repository folder
3. Run `docker-compose up -d`, this will pull the required images and start the OAuth 2.0 server network
4. Go on Docker Desktop and check that all containers are running (`oauth-2.0-server`, `mongodb`)

In the `oauth-2.0-server` container logs you should see the following message:

```shell  
$ Server is running on port 3030
$ Connected to MongoDB
```

You're all set!
# Usage of the OAuth 2.0 Server Tool

Now that the OAuth 2.0 server is up and running. You should know that there are different ways to interact with it.
## Implemented grant types

[RFC 6749](https://datatracker.ietf.org/doc/html/rfc6749.html) describes a number of grants for a client application to acquire an access token.

The following grant types are supported by [OAuth2Server](https://oauth2-server.readthedocs.io/en/latest/api/oauth2-server.html), checked boxes represent grant types supported by our **OAuth 2.0 Server Tool**:
- [ ] [Authorization Code Grant](https://oauth2-server.readthedocs.io/en/latest/model/overview.html#authorization-code-grant)
- [x] [Client Credentials Grant](https://oauth2-server.readthedocs.io/en/latest/model/overview.html#client-credentials-grant)
- [ ] [Implicit Grant](https://oauth2-server.readthedocs.io/en/latest/model/overview.html#implicit-grant)
- [ ] [Refresh Token Grant](https://oauth2-server.readthedocs.io/en/latest/model/overview.html#refresh-token-grant)
- [X] [Password Grant](https://oauth2-server.readthedocs.io/en/latest/model/overview.html#password-grant)

_**This list may be updated in the future.**_
## Obtaining an Access Token

To obtain a token you should POST a request to http://localhost:3030/oauth/token.
### Using the Client Credentials Grant

First, you need to add a **confidential client** to the mongo database, For example:
- clientId: `confidentialApplication`
- clientSecret: `topSecret`
- grants: `[client_credentials]`
- redirectUris: `[]`

Check [here](#Using-the-Embedded-Mongo-API) to see how to add a client to the database.

Now you've created a client, you can use it to obtain an **access token** using the client credentials grant.  
You need to send the following parameters in the request :
- **Headers**
  - **Content-Type:** application/x-www-form-urlencoded
- **Body**
  - `client_id=confidentialApplication`
  - `client_secret=topSecret`
  - `grant_type=client_credentials`

For example, using `curl`:
```shell  
curl http://localhost:3030/oauth/token \
-d "client_id=confidentialApplication" \    
-d "client_secret=topSecret" \    
-d "grant_type=client_credentials" \   
-H "Content-Type: application/x-www-form-urlencoded"
```  

- **Headers**
  - **Authorization**: `"Basic " + clientId:clientSecret base64'd`
    - For example `Basic Y29uZmlkZW50aWFsQXBwbGljYXRpb246dG9wU2VjcmV0` is the base64 encoding of `confidentialApplication:topSecret`
  - **Content-Type**: `application/x-www-form-urlencoded`
- **Body**
  - `grant_type=client_credentials`

Using `curl`:
```shell  
curl http://localhost:3030/oauth/token \
-d "grant_type=client_credentials" \
-H "Authorization: Basic Y29uZmlkZW50aWFsQXBwbGljYXRpb246dG9wU2VjcmV0" \
-H "Content-Type: application/x-www-form-urlencoded"
```  

The response should look like this:
```json  
{
	"access_token": "cf13c5c5c5bfc41ed33e7ff2d80cc97d989c48dc",
	"token_type": "Bearer",
	"expires_in": 86399,
	"clientId": "confidentialApplication"
}  
```  

Congrats! You've obtained your first access token using the client credentials grant.

### Using the Password Grant

To use the password grant, you need to have a user and a client in the database.
For example:
- Client :
  - clientId: `confidentialApplication`
  - clientSecret: `topSecret`
  - grants: `[password]`
  - redirectUris: `[]`
- User :
  - username: `user`
  - password: `password`

Check [here](#Using-the-Embedded-Mongo-API) to see how to add a client or a user to the database.

Now you've created a client and a user, you can use them to obtain an **access token** using the password grant.
You need to send the following parameters in the request:
- **Headers**
  - **Content-Type:** application/x-www-form-urlencoded
  - **Authorization:** `"Basic " + clientId:clientSecret base64'd`
  - **Body**
    - `grant_type=password`
    - `username=user`
    - `password=password`

For example, using `curl`:
```shell
curl http://localhost:3030/oauth/token \
-d "grant_type=password" \
-d "username=user" \
-d "password=password"
```

The response should look like this:
```json
{
    "access_token": "cf13c5c5c5bfc41ed33e7ff2d80cc97d989c48dc",
    "token_type": "Bearer",
    "expires_in": 86399,
    "clientId": "confidentialApplication"
}
```

Congrats! You've obtained your first access token using the password grant.

## Accessing a protected resource using an access token

Now you have an access token, you can use it to access the protected resource.  
To do so, you need to send a GET request to http://localhost:3030/protected with the following headers:
- **Headers**
  - **Authorization**: `"Bearer " + access_token`
    - For example `Bearer cf13c5c5c5bfc41ed33e7ff2d80cc97d989c48dc`

Using `curl`:

```shell  
curl http://localhost:3030/protected \
-H "Authorization: Bearer cf13c5c5c5bfc41ed33e7ff2d80cc97d989c48dc"  
```  

If the access token is valid, you should get the following response:

```json  
{
	"success": true
}  
```  

Else an error message will be returned.
# Using the Embedded Mongo API

The OAuth 2.0 server tool comes with an embedded mongo database that you can use to add clients and users.
Some endpoints are available to interact with the database.
## Clients endpoints
| Endpoint                                | Method | Description                                              |  
|-----------------------------------------|--------|----------------------------------------------------------|  
| /mongo-api/client/findAll                  | GET    | Get all clients                                          |  
| /mongo-api/client/findByClientId/:clientId | GET    | Get a client by its clientId                             |  
| /mongo-api/client/createClient             | POST   | Create a new client, send error if client already exists |  
| /mongo-api/client/updateClient             | PUT    | Update a client, send error if client doesn't exist      |  
## Users endpoints
| Endpoint                              | Method  | Description                                          |  
|---------------------------------------|---------|------------------------------------------------------|  
| /mongo-api/user/findAll                  | GET     | Get all users                                        |  
| /mongo-api/user/findByUsername/:username | GET     | Get a user by its username                           |  
| /mongo-api/user/createUser               | POST    | Create a new user, send error if user already exists |  
| /mongo-api/user/updateUser               | PUT     | Update a user, send error if user doesn't exist      |  
## Tokens endpoints
| Endpoint                                        | Method | Description                                 |  
|-------------------------------------------------|--------|---------------------------------------------|  
| /mongo-api/token/findAll                        | GET    | Get all tokens                              |  
| /mongo-api/token/findByAccessToken/:accessToken | GET    | Get a token by its accessToken              |  
| /mongo-api/token/findByClientId/:clientId       | GET    | Get all tokens of a client by its clientId  |  
| /mongo-api/token/findExpiredTokens              | GET    | Get all expired tokens                      |  
| /mongo-api/token/deleteExpiredTokens            | DELETE | Delete all expired tokens                   |  

## Some examples
### Create a client

To create a client, you need to send a POST request to http://localhost:3030/mongo-api/client/createClient with the following body:

```json  
{  
	"clientId": <clientId>,
	"clientSecret": <clientSecret>,
	"grants": [<grants>],
	"redirectUris": [<redirectUris>]
}  
```  

For example, using `curl`:

```shell  
curl -X POST http://localhost:3030/mongo-api/client/createClient \
	-H 'Content-Type: application/json' \  
	-d '{ "clientId": "confidentialApplication", "clientSecret": "topSecret", "grants": ["client_credentials", "password"], "redirectUris": [] }'
```

### Update a client

To update a client, you need to send a PUT request to http://localhost:3030/mongo-api/client/updateClient with the following body:

```json  
{  
	"clientId": <clientId>,
	"clientSecret": <clientSecret>,
	"grants": [<grants>],
	"redirectUris": [<redirectUris>]
}  
```  

For example, using `curl`:

```shell  
curl -X PUT http://localhost:3030/mongo-api/client/updateClient \  
	-H 'Content-Type: application/json' \
	-d '{ "clientId": "confidentialApplication", "clientSecret": "topSecret", "grants": ["client_credentials", "password"], "redirectUris": [] }'
```

### Create a user

To create a user, you need to send a POST request to http://localhost:3030/mongo-api/user/createUser with the following body:

```json  
{  
    "username": <username>,
    "password": <password>
}  
```

For example, using `curl`:

```shell
curl -X POST http://localhost:3030/mongo-api/user/createUser \
    -H 'Content-Type: application/json' \
    -d '{ "username": "user", "password": "password" }'
```

### Update a user

To update a user, you need to send a PUT request to http://localhost:3030/mongo-api/user/updateUser with the following body:

```json  
{  
    "username": <username>,
    "password": <password>
}  
```

For example, using `curl`:

```shell
curl -X PUT http://localhost:3030/mongo-api/user/updateUser \
    -H 'Content-Type: application/json' \
    -d '{ "username": "user", "password": "password" }'
```