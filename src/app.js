const express = require('express');

const app = express();

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const debugControl = require('./utilities/debug.js');
app.use(debugControl.log.request());

/**
 * Mongo API routes
 */
const userRoutes = require('./mongo/routes/userRoutes');
const clientRoutes = require('./mongo/routes/clientRoutes');
const tokenRoutes = require('./mongo/routes/tokenRoutes');

app.use('/mongo-api/user', userRoutes);
app.use('/mongo-api/client', clientRoutes);
app.use('/mongo-api/token', tokenRoutes);

/**
 * Authentication server and routes
 */
const oauthServer = require('./server/server.js');
const oAuthRoutes = require('./server/routes/oAuthRoutes');

const protectedRoutes = require('./server/routes/protectedRoutes');
app.use('/oauth', oAuthRoutes) // routes to access the auth stuff

// Note that the next router uses middleware. That protects all routes within this middleware
app.use('/protected', (req,res,next) => {
    debugControl.log.flow('Authentication')
    return next()
},oauthServer.authenticate(), protectedRoutes) // routes to access the protected stuff

app.use('/', (req,res) => res.redirect('/client'))

module.exports = app;
