const OAuthServer = require('express-oauth-server')
const config = require('../config/config');
const model = require('./model')

module.exports = new OAuthServer({
    model: model,
    grants: ['client_credentials', "password"],
    accessTokenLifetime: config.server.ACCESS_TOKEN_LIFETIME,
    allowEmptyState: true,
    allowExtendedTokenAttributes: true,
})
