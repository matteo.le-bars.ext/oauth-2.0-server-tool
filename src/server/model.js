const debugControl = require('../utilities/debug');

const clientModel = require('../mongo/models/clientModel');
const userModel = require('../mongo/models/userModel');
const tokenModel = require('../mongo/models/tokenModel');

// Methods required by all grant types.
/**
 * Get access token.
 *
 * Documentation: https://oauth2-server.readthedocs.io/en/latest/model/spec.html#getaccesstoken-accesstoken-callback
 */
module.exports.getAccessToken = function (bearerToken) {
    log({
        title: 'Get Access Token',
        parameters: [
            { name: 'bearerToken', value: bearerToken },
        ]
    })

    try {
        const token = tokenModel.findOne({ accessToken: bearerToken }).lean();
        return new Promise(resolve => resolve(token));
    }
    catch (error) {
        console.log('Error getting access token', error);
    }
}

/**
 * Get client.
 *
 * Documentation: https://oauth2-server.readthedocs.io/en/latest/model/spec.html#getclient-clientid-clientsecret-callback
 */
module.exports.getClient = async function (clientId, clientSecret) {
    log({
        title: 'Get Client',
        parameters: [
            { name: 'clientId', value: clientId },
            { name: 'clientSecret', value: clientSecret },
        ]
    })

    try {
        const client = await clientModel.findOne({ clientId: clientId, clientSecret: clientSecret }).lean();
        return new Promise(resolve => resolve(client));
    }
    catch (error) {
        console.log('Error getting client', error);
    }
}

/**
 * Save token.
 *
 * Documentation: https://oauth2-server.readthedocs.io/en/latest/model/spec.html#savetoken-token-client-user-callback
 */
module.exports.saveToken = async function (token, client, user) {
    log({
        title: 'Saving Token',
        parameters: [
            { name: 'token', value: token },
            { name: 'client', value: client },
            { name: 'user', value: user },
        ]
    })

    try {
        const newToken = new tokenModel({
            accessToken: token.accessToken,
            accessTokenExpiresOn: token.accessTokenExpiresOn,
            client : client,
            clientId: client.clientId,
            refreshToken: token.refreshToken,
            refreshTokenExpiresOn: token.refreshTokenExpiresOn,
            user : user,
            userId: user._id,
        });

        let savedResult = await newToken.save();

        savedResult = savedResult.toJSON();

        delete savedResult._id;
        delete savedResult.__v;

        return new Promise(resolve => resolve(savedResult));
    }
    catch (error) {
        console.log('Error saving token', error);
    }
}

// Methods required only by Client Credentials grant type.
/**
 * Get User From Client. The user object is completely transparent to oauth2-server and is simply used as input to other model functions.
 * For our purposes, we will simply return an empty object.
 *
 * Documentation: https://oauth2-server.readthedocs.io/en/latest/model/spec.html#getuserfromclient-client-callback
 */
module.exports.getUserFromClient = function (client) {
    return { username: "" };
}

// Methods required only by Password grant type.
/**
 * Get user.
 *
 * Documentation: https://oauth2-server.readthedocs.io/en/latest/model/spec.html#model-getuser
 */
module.exports.getUser = async function (username, password) {
    log({
        title: 'Get User',
        parameters: [
            { name: 'username', value: username },
            { name: 'password', value: password },
        ]
    })

    try {
        const user = await userModel.findOne({username: username, password: password}).lean();
        return new Promise(resolve => resolve(user));
    }
    catch (error) {
        console.log('Error getting user', error);
    }
}


// Function to render the log messages.
function log({ title, parameters }) {
    debugControl.log.functionName(title)
    debugControl.log.parameters(parameters)
}
