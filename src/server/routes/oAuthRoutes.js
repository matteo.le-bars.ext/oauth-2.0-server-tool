const express = require('express')
const oauthServer = require('../server.js')

const DebugControl = require('../../utilities/debug.js')


const router = express.Router() // Instantiate a new router

router.post('/token', (req,res,next) => {
    DebugControl.log.flow('Token')
    next()
},oauthServer.token({
    requireClientAuthentication: { // whether client needs to provide client_secret
        // 'authorization_code': false,
    },
}))  // Sends back token

module.exports = router
