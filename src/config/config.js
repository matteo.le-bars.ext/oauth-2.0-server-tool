module.exports = {
    server: {
        PORT: 3030,
        ACCESS_TOKEN_LIFETIME: 60 * 15, // 15 minutes
    },
    mongo: {
        MONGO_URI: process.env.MONGO_URI || 'mongodb://root:VerySecurePassword@localhost:27017',
    },
}