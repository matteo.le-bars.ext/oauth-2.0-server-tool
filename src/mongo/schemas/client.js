module.exports = {
    clientId: { type: String },
    clientSecret: { type: String },
    grants: [String],
    redirectUris: { type: Array }
};
