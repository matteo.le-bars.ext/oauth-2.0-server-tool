const express = require('express')
const clientController = require('../controllers/clientController');

const router = express.Router();

router.get('/findAll', clientController.findAll);

router.get('/findByClientId/:clientId', clientController.findByClientId);

router.post('/createClient', clientController.createClient);

router.put('/updateClient', clientController.updateClient);

module.exports = router;
