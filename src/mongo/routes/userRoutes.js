const express = require('express')
const userController = require('../controllers/userController');

const router = express.Router();

router.get('/findAll', userController.findAll);

router.get('/findByUsername/:username', userController.findByUsername);

router.post('/createUser', userController.createUser);

router.put('/updateUser', userController.updateUser);

module.exports = router;
