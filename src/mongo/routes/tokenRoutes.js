const express = require('express')
const tokenController = require('../controllers/tokenController');

const router = express.Router();

router.get('/findAll', tokenController.findAll);

router.get('/findByToken/:token', tokenController.findByToken);

router.get('/findByClientId/:clientId', tokenController.findByClientId);

router.get('/findExpiredTokens', tokenController.findExpiredTokens);

router.delete('/deleteExpiredTokens', tokenController.deleteExpiredTokens);

module.exports = router;
