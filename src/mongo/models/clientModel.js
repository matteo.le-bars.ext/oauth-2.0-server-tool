const mongoose = require('../../config/databaseConfig')

const modelName = 'client'

const schemaDefinition = require('../schemas/' + modelName);

const schemaInstance = new mongoose.Schema(schemaDefinition);

const modelInstance = mongoose.model(modelName, schemaInstance);

module.exports = modelInstance;
