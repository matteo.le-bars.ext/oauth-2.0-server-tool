const mongoose = require('../../config/databaseConfig')

const modelName = 'user';

const schemaDefinition = require('../schemas/' + modelName);

const schemaInstance = new mongoose.Schema(schemaDefinition);

const modelInstance = mongoose.model(modelName, schemaInstance);

module.exports = modelInstance;
