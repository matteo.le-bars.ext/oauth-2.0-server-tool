const mongoose = require('../../config/databaseConfig')

const modelName = 'token';

const schemaDefinition = require('../schemas/' + modelName);

const schemaInstance = new mongoose.Schema(schemaDefinition);

const modelInstance = mongoose.model(modelName, schemaInstance);

module.exports = modelInstance;
