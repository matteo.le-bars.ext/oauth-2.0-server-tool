const clientModel = require('../models/clientModel');

exports.findAll = async (req, res) => {
    try {
        const clients = await clientModel.find({});
        res.status(200).json(clients);
    } catch (error) {
        res.status(500).json(error);
    }
}

exports.findByClientId = async (req, res) => {
    try {
        const client = await clientModel.findOne({ clientId: req.params.clientId });
        res.status(200).json(client);
    } catch (error) {
        res.status(500).json(error);
    }
}

exports.createClient = async (req, res) => {
    const { clientId, clientSecret, grants, redirectUris } = req.body;

    try {
        // Check for duplicate
        const existingClient = await clientModel.findOne({ clientId: clientId });

        if (existingClient) {
            return res.status(400).json({ error: 'Duplicate entry: This client already exists, call update instead' });
        }

        const newClient = new clientModel({ clientId, clientSecret, grants, redirectUris });
        await newClient.save();
        return res.status(201).json(newClient);
    } catch (error) {
        res.status(500).json(error);
    }
}

exports.updateClient = async (req, res) => {
    const { clientId, clientSecret, grants, redirectUris } = req.body;

    try {
        const client = await clientModel.findOne({ clientId });

        if (!client) {
            return res.status(400).json({ error: 'Client not found' });
        }

        client.clientSecret = clientSecret;
        client.grants = grants;
        client.redirectUris = redirectUris;

        await client.save();
        return res.status(200).json(client);
    } catch (error) {
        res.status(500).json(error);
    }
}
