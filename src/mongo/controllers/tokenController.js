const tokenModel = require('../models/tokenModel');

exports.findAll = async (req, res) => {
    try {
        const tokens = await tokenModel.find({});
        res.status(200).json(tokens);
    } catch (error) {
        res.status(500).json(error);
    }
}

exports.findByToken = async (req, res) => {
    try {
        const token = await tokenModel.findOne({ token: req.params.token });
        res.status(200).json(token);
    } catch (error) {
        res.status(500).json(error);
    }
}

exports.findByClientId = async (req, res) => {
    try {
        const tokens = await tokenModel.find({ clientId: req.params.clientId });
        res.status(200).json(tokens);
    } catch (error) {
        res.status(500).json(error);
    }
}

exports.findExpiredTokens = async (req, res) => {
    try {
        const tokens = await tokenModel.find({ accessTokenExpiresOn: { $lt: new Date() } });
        res.status(200).json(tokens);
    } catch (error) {
        res.status(500).json(error);
    }
}

exports.deleteExpiredTokens = async (req, res) => {
    try {
        await tokenModel.deleteMany({ accessTokenExpiresOn: { $lt: new Date() } });
        res.status(200).json({ message: 'Expired tokens deleted' });
    } catch (error) {
        res.status(500).json(error);
    }
}
