const userModel = require('../models/userModel');

exports.findAll = async (req, res) => {
    try {
        const users = await userModel.find({});
        res.status(200).json(users);
    } catch (error) {
        res.status(500).json(error);
    }
}

exports.findByUsername = async (req, res) => {
    try {
        const user = await userModel.findOne({ username: req.params.username });
        res.status(200).json(user);
    } catch (error) {
        res.status(500).json(error);
    }
}

exports.createUser = async (req, res) => {
    const { username, password } = req.body;

    try {
        // Check for duplicate
        const existingUser = await userModel.findOne({ username });

        if (existingUser) {
            return res.status(400).json({ error: 'Duplicate entry: This user already exists, call update instead' });
        }

        const newUser = new userModel({ username, password });
        await newUser.save();
        return res.status(201).json(newUser);
    } catch (error) {
        res.status(500).json(error);
    }
}

exports.updateUser = async (req, res) => {
    const { username, password } = req.body;

    try {
        const user = await userModel.findOne({ username });

        if (!user) {
            return res.status(400).json({ error: 'User not found' });
        }

        user.password = password;

        await user.save();

        return res.status(200).json(user);
    }
    catch (error) {
        res.status(500).json(error);
    }
}